//
//  Constants.swift
//  20200812-VasishtaKalinadhabhotla-NYCSchools
//
//  Created by Vasishta Kalinadhabhotla on 8/12/20.
//  Copyright © 2020 Vasishta Kalinadhabhotla. All rights reserved.
//

import Foundation

struct Constants {
    // URLs
    static let fetchSchoolsUrl: String = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let fetchSatResultsUrl: String = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    // ViewControllers
    static let schoolListVC: String = "SchoolListViewController"
    static let detailVC: String = "DetailViewController"
}
