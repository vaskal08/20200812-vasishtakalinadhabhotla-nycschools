//
//  SchoolListViewController.swift
//  20200812-VasishtaKalinadhabhotla-NYCSchools
//
//  Created by Vasishta Kalinadhabhotla on 8/12/20.
//  Copyright © 2020 Vasishta Kalinadhabhotla. All rights reserved.
//

import UIKit

class SchoolListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var loadingView: UIAlertController?
    
    private var schools: Array<School> = Array()
    private var satResults: Array<SATResult> = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.fetchData()
    }
    
    private func fetchData() {
        // Display loading view on screen, and proceed to call APIClient to fetch data
        self.displayLoading()
        
        APIClient.fetchSchools { (schools: Array<School>?) in
            // Verify that schools list is not nil.
            // We assume nil object for the results was caused by an error upstream in the logic
            
            guard let schoolList = schools else {
                DispatchQueue.main.async {
                    // Display error message on screen. stopDisplayingLoading() is UI related, so we should execute on main thread
                    self.stopDisplayingLoading(loadingFailed: true)
                }
                
                return
            }
            
            // When we are done fetching schools, we can move on to fetching SAT results, as it is dependent on the schools.
            // I would have liked to use Swift Operations here to create a dependency in an OperationQueue, but with the interest of time, I went this way since it is only two API calls. With a larger number of calls, OperationQueue would be a better route.
            
            APIClient.fetchSATResults { (results: Array<SATResult>?) in
                // Verify that SAT results are not nil.
                // We assume nil object for the results was caused by an error upstream in the logic
                
                guard let satResults = results else {
                    print ("error fetching SAT results")
                    
                    DispatchQueue.main.async {
                        // Display error message on screen. stopDisplayingLoading() is UI related, so we should execute on main thread
                        self.stopDisplayingLoading(loadingFailed: true)
                    }
                    
                    return
                }
                
                
                // Once we have the required data, store them globally and reload update the UI
                self.schools = schoolList
                self.satResults = satResults
                
                // Reload UITableView and remove loading indicator in main thread
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.stopDisplayingLoading()
                }
            }
        }
    }
    
    // MARK: - UI Related Methods
    /*
     * UI methods are kept separate to be able to call them conveniently in the main thread
     */
    
    // Display a loading message with a basic UIAlertController. It is stored globally to later dismiss it.
    // Given more time, it would be better to create something with animation to give the user a sense of progress.
    private func displayLoading() {
        self.loadingView = UIAlertController(title: "Fetching Data...", message: nil, preferredStyle: .alert)
        self.present(self.loadingView!, animated: true, completion: nil)
    }
    
    // Remove the loading indicator. If there was an error loading, call handleError() to display an error message.
    private func stopDisplayingLoading(loadingFailed: Bool=false) {
        if let loadingView = self.loadingView {
            loadingView.dismiss(animated: true) {
                if loadingFailed {
                    self.handleError()
                }
                
                self.loadingView = nil
            }
        }
    }
    
    /*
     * This method serves as the central point to come to when an error is encountered. It displays a generic message to prompt the user to try again.
     * Other parts of the code check for errors through nil checks/try,catch to separate the business logic.
     */
    private func handleError() {
        let alert = UIAlertController(title: "Error", message: "Could not process your request at this time. Please try again later.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - UITableViewDataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // Reuse dequeued basic subtitle UITableView cells with reuse identifier 'SchoolCell' which is defined in the storyboard
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell")
        
        if cell == nil {
            cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "SchoolCell")
        }
        
        let school = schools[indexPath.row]
        
        cell?.textLabel?.text = school.schoolName
        cell?.detailTextLabel?.text = "\(school.finalGrades) | \(school.totalStudents) students"
        
        return cell!
    }
    
    // This method is called by the UIBarButton in the view controller
    // re-uses the fetchData() method after clearing the tableView and schools, satResults variables
    @IBAction func refresh(_ sender: Any) {
        self.schools = Array<School>()
        self.satResults = Array<SATResult>()
        
        self.tableView.reloadData()
        
        self.fetchData()
    }
    
    // MARK: - UITableViewDelegate Methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = self.schools[indexPath.row]
        let satResult = APIClient.satResultForDbn(dbn: school.dbn, satResults: self.satResults)
        
        if let detailViewController = self.storyboard?.instantiateViewController(withIdentifier: Constants.detailVC) as? DetailViewController {
            
            detailViewController.school = school
            detailViewController.satResult = satResult
            
            self.navigationController?.pushViewController(detailViewController, animated: true)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
