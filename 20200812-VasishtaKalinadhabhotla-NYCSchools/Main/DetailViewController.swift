//
//  DetailViewController.swift
//  20200812-VasishtaKalinadhabhotla-NYCSchools
//
//  Created by Vasishta Kalinadhabhotla on 8/13/20.
//  Copyright © 2020 Vasishta Kalinadhabhotla. All rights reserved.
//

import UIKit


// All UX layout for this view is handled using AutoLayout in the Storyboard. Given time I would have like to add a scroll view to accomodate for smaller screens and more details in the future.
class DetailViewController: UIViewController {
    
    var school: School!
    var satResult: SATResult?

    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolDetailLabel: UILabel!
    @IBOutlet weak var schoolPhoneButton: UIButton!
    
    @IBOutlet weak var satResultsHeader: UILabel!
    @IBOutlet weak var satResultsSection: UIView!
    
    // Given more time I would have liked to abstract these details into a separate UIView with a class and xib as to not clutter this view controller.
    @IBOutlet weak var satNumTestTakers: UILabel!
    @IBOutlet weak var satCriticalReading: UILabel!
    @IBOutlet weak var satMath: UILabel!
    @IBOutlet weak var satWriting: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = school.schoolName
        
        self.fillData()
    }
    
    private func fillData() {
        // Fill UX labels and buttons with appropriate values
        
        schoolNameLabel.text = school.schoolName
        schoolDetailLabel.text = "\(school.finalGrades) | \(school.totalStudents) students"
        
        if let phone = school.phone {
            schoolPhoneButton.setTitle("Phone: \(phone)", for: .normal)
            schoolPhoneButton.isEnabled = true
        } else {
            schoolPhoneButton.setTitle("", for: .normal)
            schoolPhoneButton.isEnabled = false
        }
        
        // Fill SAT results data if available.
        // If SAT results are not available, remove the section for SAT results, and edit the header to tell user that data is not available.
        if let sat = self.satResult {
            satResultsHeader.text = "SAT Results"
            
            satResultsSection.isHidden = false
            
            satNumTestTakers.text = "Num Test Takers: \(sat.numSatTestTakers)"
            satCriticalReading.text = "Avg Critical Reading Score: \(sat.avgCriticalReading)"
            satMath.text = "Avg Math Score: \(sat.avgMath)"
            satWriting.text = "Avg Writing Score: \(sat.avgWriting)"
        } else {
            satResultsHeader.text = "Could not find SAT results for this school."
            satResultsSection.isHidden = true
        }
    }
    @IBAction func callSchool(_ sender: Any) {
        // Check if phone number exists, and can be opened
        // Note: this will not work in simulator, as there is no phone app
        if
            let phone = school.phone,
            let url = URL(string: "tel://\(phone)"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            // Handle if phone number cannot be opened by presenting an error message.
            
            let alert = UIAlertController(title: "Error", message: "Could not call the phone number you have requested.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}
