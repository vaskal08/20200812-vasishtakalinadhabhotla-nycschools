//
//  RestClient.h
//  20200812-VasishtaKalinadhabhotla-NYCSchools
//
//  Created by Vasishta Kalinadhabhotla on 8/12/20.
//  Copyright © 2020 Vasishta Kalinadhabhotla. All rights reserved.
//

#ifndef RestClient_h
#define RestClient_h

#import <Foundation/Foundation.h>

@interface RestClient : NSObject

typedef NS_ENUM(NSUInteger, RestMethod) {
    GET=0,
    POST=1
};

+ (void) performRequest:(NSURL *) url method: (RestMethod) method completion:(void(^)(NSData *, NSError *)) completion;
+ (NSString *) restMethodToString: (RestMethod) method;

@end


#endif /* RestClient_h */
