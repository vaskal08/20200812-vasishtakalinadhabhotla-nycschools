//
//  APIClient.swift
//  20200812-VasishtaKalinadhabhotla-NYCSchools
//
//  Created by Vasishta Kalinadhabhotla on 8/12/20.
//  Copyright © 2020 Vasishta Kalinadhabhotla. All rights reserved.
//

import Foundation

class APIClient {
    
    // APIClient does the job of fetching specific data using the REST Client
    // The REST calls are made asynchronously through GCD with both calls having user initiated QoS
    // This insures high priority for these calls as the user is waiting for the results to start using the app.
    
    static func fetchSchools(completion: @escaping (Array<School>?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            guard let url = URL(string: Constants.fetchSchoolsUrl) else {
                completion(nil)
                
                return
            }
            
            // Call RestClient perform request and wait for result.
            RestClient.performRequest(url, method: RestMethod.GET) { (response: Data?, error: Error?) in
                
                // Error is handled here. The existence of an error is assumed to mean data did not come with integrity, so we send back nil response to be handled by UI message to user
                if let e = error {
                    print (String(describing: e))
                    
                    completion(nil)
                    return
                }
                
                // We do a second check to make sure we have data. If not we send back nil response
                guard let responseData = response else {
                    completion(nil)
                    return
                }
                
                // We use the convenience of defining our models by the Decodable protocol to use JSONDecoder. If it is successful, we can return the records, otherwise return nil to allow error to be handled by UI.
                do {
                    let schools = try JSONDecoder().decode(Array<School>.self, from: responseData)
                
                    completion(schools)
                } catch {
                    completion(nil)
                }
            }
        }
    }
    
    // The following method has similar structure to fetchSchools() so the notes are the same
    static func fetchSATResults(completion: @escaping (Array<SATResult>?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            guard let url = URL(string: Constants.fetchSatResultsUrl) else {
                completion(nil)
                
                return
            }
            
            RestClient.performRequest(url, method: RestMethod.GET) { (response: Data?, error: Error?) in
                // Handle errors
                
                if let e = error {
                    print (String(describing: e))
                    
                    completion(nil)
                    return
                }
                
                guard let responseData = response else {
                    completion(nil)
                    return
                }
                
                // Parse to model
                do {
                    let results = try JSONDecoder().decode(Array<SATResult>.self, from: responseData)
                
                    completion(results)
                } catch {
                    print (error)
                    completion(nil)
                }
            }
        }
    }
    
    // Utiltity functions for models
    
    // I would like to have these utility functions in a dedicated class, but did not want to overcomplicate the project as there is only one method
    static func satResultForDbn(dbn: String, satResults: Array<SATResult>) -> SATResult? {
        for result in satResults {
            
            if result.dbn == dbn {
                return result
            }
        }
        
        return nil
    }
}
