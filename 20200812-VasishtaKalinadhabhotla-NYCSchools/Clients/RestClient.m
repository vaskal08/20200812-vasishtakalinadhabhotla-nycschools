//
//  RestClient.m
//  20200812-VasishtaKalinadhabhotla-NYCSchools
//
//  Created by Vasishta Kalinadhabhotla on 8/12/20.
//  Copyright © 2020 Vasishta Kalinadhabhotla. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestClient.h"

@implementation RestClient

/*
 * RestClient was written in Objective-C to demonstrage it's usage, and to demontrate compatibility with Swift through the bridging header.
 *
 * RestClient is separate from APIClient to abstract the usage of REST services and simplify the APIClient.
 *
 * The RestClient itself does not perform the REST request asynchronously.
 * The class that uses this, APIClient, handles the multithreading usage of this method through GCD in Swift.
 */

+ (void) performRequest:(NSURL *) url method: (RestMethod) method completion:(void(^)(NSData *, NSError *)) completion {
    // Use a semaphore to allow the method to wait for data to come back from server
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
      cachePolicy:NSURLRequestUseProtocolCachePolicy
      timeoutInterval:60.0];

    // Parse enum to string supported by NSURLSessionDataTask
    [request setHTTPMethod:[RestClient restMethodToString: method]];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        // pass along data and error to the requestor.
        // Errors will be handled by the APIClient
        completion(data, error);
        
        dispatch_semaphore_signal(semaphore);
    }];
    
    [dataTask resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}

+ (NSString *) restMethodToString: (RestMethod) method {
    switch (method) {
        case GET:
            return @"GET";
        case POST:
            return @"POST";
        default:
            return @"GET";
    }
}

@end
