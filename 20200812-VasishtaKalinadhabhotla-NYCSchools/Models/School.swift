//
//  School.swift
//  20200812-VasishtaKalinadhabhotla-NYCSchools
//
//  Created by Vasishta Kalinadhabhotla on 8/12/20.
//  Copyright © 2020 Vasishta Kalinadhabhotla. All rights reserved.
//

import Foundation

struct School: Decodable {
    var dbn: String
    var schoolName: String
    var finalGrades: String
    var totalStudents: String
    var phone: String?
    
    /* I didn't place all the available columns for this model because there are a lot, and only decided to include those that I use/display.
     * Any new fields can be added here and used as needed
     */
    
    // Added coding keys so that we can avoid using underscores in the variable names, as camel case is the standard with Swift
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName="school_name"
        case finalGrades="finalgrades"
        case totalStudents="total_students"
        case phone="phone_number"
    }
}
