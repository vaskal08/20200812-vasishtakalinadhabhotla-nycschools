//
//  SATResult.swift
//  20200812-VasishtaKalinadhabhotla-NYCSchools
//
//  Created by Vasishta Kalinadhabhotla on 8/12/20.
//  Copyright © 2020 Vasishta Kalinadhabhotla. All rights reserved.
//

import Foundation

struct SATResult: Decodable {
    var dbn: String
    var schoolName: String
    var numSatTestTakers: String
    var avgCriticalReading: String
    var avgMath: String
    var avgWriting: String
    
    // Added coding keys so that we can avoid using underscores in the variable names, as camel case is the standard with Swift
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numSatTestTakers = "num_of_sat_test_takers"
        case avgCriticalReading = "sat_critical_reading_avg_score"
        case avgMath = "sat_math_avg_score"
        case avgWriting = "sat_writing_avg_score"
    }
}
