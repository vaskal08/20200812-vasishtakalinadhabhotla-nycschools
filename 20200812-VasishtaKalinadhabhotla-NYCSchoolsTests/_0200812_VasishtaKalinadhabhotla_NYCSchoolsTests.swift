//
//  _0200812_VasishtaKalinadhabhotla_NYCSchoolsTests.swift
//  20200812-VasishtaKalinadhabhotla-NYCSchoolsTests
//
//  Created by Vasishta Kalinadhabhotla on 8/12/20.
//  Copyright © 2020 Vasishta Kalinadhabhotla. All rights reserved.
//

import XCTest
@testable import _0200812_VasishtaKalinadhabhotla_NYCSchools

class _0200812_VasishtaKalinadhabhotla_NYCSchoolsTests: XCTestCase {
    
    var satResults: Array<SATResult>?
    var schools: Array<School>?

    override func setUp() {
        let bundle = Bundle(for: type(of: self))
        if let satPath = bundle.path(forResource: "sampleSatResults", ofType: "json") {
            do {
                let satSampleData = try Data(contentsOf: URL(fileURLWithPath: satPath))
                satResults = try JSONDecoder().decode(Array<SATResult>.self, from: satSampleData)
            } catch {
                satResults = nil
            }
        }
    }

    override func tearDown() {
        satResults = nil
    }
    
    func testSatResultFromDbn() {
        guard let results = satResults else {
            XCTFail()
            
            return
        }
        
        // Test that result with valid dbn isn't null, and correct values are returned
        let result1 = APIClient.satResultForDbn(dbn: "01M509", satResults: results)
        XCTAssertNotNil(result1)
        XCTAssertEqual(result1!.schoolName, "MARTA VALLE HIGH SCHOOL")
        XCTAssertEqual(result1!.avgCriticalReading, "390")
        
        let result2 = APIClient.satResultForDbn(dbn: "01M696", satResults: results)
        XCTAssertNotNil(result2)
        XCTAssertEqual(result2!.schoolName, "BARD HIGH SCHOOL EARLY COLLEGE")
        XCTAssertEqual(result2!.avgCriticalReading, "624")
        
        
        // Dbn value doesn't exist, result should be nil
        let result3 = APIClient.satResultForDbn(dbn: "sample_error", satResults: results)
        XCTAssertNil(result3)
        
    }
    
    
    // Test to make sure our helper method for the RestMethod enum translates to String understood by NSURLSession
    // This would be more useful as more methods are added to the enum
    func testRestMethodToString() {
        let method1 = RestMethod.GET
        let method2 = RestMethod.POST
                
        XCTAssertEqual(RestClient.restMethod(toString: method1), "GET")
        XCTAssertEqual(RestClient.restMethod(toString: method2), "POST")
    }
}
